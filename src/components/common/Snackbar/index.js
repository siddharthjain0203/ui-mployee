import React from 'react';
import { connect } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import { makeStyles } from '@material-ui/core/styles';
import { closeSnackabr } from '../../../store/actionsCreator/snackbarActionCreator';


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

function CustomizedSnackbars(props) {
    const { openSnackbar, variantIcon, closeSnackabr, message } = props;
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Snackbar
                anchorOrigin={{ vertical:'top', horizontal:'center' }}
                open={openSnackbar}
                onClose={closeSnackabr}
                message={message}
                key={1}
                autoHideDuration={6000}
            />
        </div>
    );
}
const mapPropsToState = ({ snackbar }) => ({
    openSnackbar: snackbar.open,
    variantIcon: snackbar.variantIcon,
    message: snackbar.message
})

export default connect(mapPropsToState, { closeSnackabr })(CustomizedSnackbars);