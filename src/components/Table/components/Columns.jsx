import React from 'react';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import DeleteIcon from '@material-ui/icons/DeleteOutlineRounded'

export const ColumnsCells = props => {
    const { columns, onHeader } = props
    return (
        <TableHead >
            <TableRow>
                <TableCell key={-1} align="center" onClick={onHeader}><DeleteIcon/></TableCell>
                {columns.map((cell, index) => {
                    if (cell.visibility) {
                        return (
                            <TableCell
                            key={index}
                            align={cell.align}
                            style={{ minWidth: cell.minWidth }}
                            >
                                {cell.label}
                            </TableCell>)
                    } else return null
                })}
                <TableCell key={-2} align="center" >Action</TableCell>
            </TableRow>
        </TableHead>
    )
}
