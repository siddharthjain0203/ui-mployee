
import { SET_AUTHENTICATION, SET_REDIRECT_URL } from '../actionTypes/authTypes'
export const setRedirectUrl = url => ({
    type: SET_REDIRECT_URL,
    payload: {
        url
    }
});

export const setAuthLogin = status => ({
    type: SET_AUTHENTICATION,
    payload:  status 
});
