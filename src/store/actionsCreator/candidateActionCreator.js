
import * as CD from '../actionTypes/candidateType'

export const setCandidateList = data => ({
    type: CD.CANDIDATE_LIST,
    payload: data
});

