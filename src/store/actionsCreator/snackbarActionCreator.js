import * as SNACKBARTYPE from '../actionTypes/snackbarTypes';

export const openSnackbar = (variantIcon, message) => {
    return {
        type: SNACKBARTYPE.OPEN_SNACKBAR,
        payload: {
            variantIcon, message
        }
    }
}

export const closeSnackabr = () => {
    return {
        type: SNACKBARTYPE.CLOSE_SNACKBAR
    }
} 
