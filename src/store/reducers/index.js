import { combineReducers } from "redux";
import auth from "./auth";
import snackbarReducer from "./snackbarReducer";
import candidateReducer from "./candidateReducer";

let rootReducer = combineReducers({
    auth,
    candidate:candidateReducer,
    snackbar: snackbarReducer,
});

export default rootReducer;