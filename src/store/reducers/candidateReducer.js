import  * as CD from "../actionTypes/candidateType"
import { candidates } from "../../data/candidates";

const initialState = {
    candidates: candidates
};

const candidateReducer = (state = initialState, action) => {
    switch (action.type) {
        case CD.CANDIDATE_LIST: {
            return {
                ...state,
                candidates: action.payload
            };
        }
        default:
            return state;
    }
}

export default candidateReducer;