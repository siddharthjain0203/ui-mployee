import * as SNACKBARTYPE from '../actionTypes/snackbarTypes';

const initialState = {
    open: false,
    variantIcon: "",
    message: ""
}

const snackbarReducer = (state = initialState, action) => {
    switch (action.type) {
        case SNACKBARTYPE.OPEN_SNACKBAR:
            return {
                ...state,
                variantIcon: action.payload.variantIcon,
                message: action.payload.message,
                open: true
            }
        case SNACKBARTYPE.CLOSE_SNACKBAR:
            return {
                ...state,
                ...initialState
            }

        default:
            return state
    }
}

export default snackbarReducer;