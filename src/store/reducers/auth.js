import { SET_AUTHENTICATION, SET_REDIRECT_URL } from "../actionTypes/authType"

const initialState = {
    redirectUrl: "",
    isAuthenticated: false
};

const auth = (state = initialState, action) => {
    switch (action.type) {
        case SET_AUTHENTICATION: {
            return {
                ...state,
                isAuthenticated: action.payload
            };
        }
        case SET_REDIRECT_URL: {
            const { url } = action.payload
            return {
                ...state,
                url: url
            };
        }
        default:
            return state;
    }
}

export default auth;