import { createStore, compose, applyMiddleware } from "redux"
import { persistStore, persistReducer } from "redux-persist"
import storage from "redux-persist/lib/storage"
import rootReducer from "./reducers"
import thunk from 'redux-thunk';

const persistConfig = {
  key: "root",
  storage: storage
}

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  persistReducer(persistConfig, rootReducer),
  composeEnhancer(applyMiddleware(thunk))
)
export const persistor = persistStore(store);
export default store  