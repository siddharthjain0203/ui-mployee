export const candidates = [
    {
        id: 1,
        name: "Jhon Smith",
        designation: "Senior UI Specialist",
        technologies: "XD,Sketch,Figma",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Indore/Banglore",
        openRelocate: "Yes",
        message : ""
    },
    {
        id: 2,
        name: "Jhon Smith",
        designation: "Senior UI Specialist",
        technologies: "Photoshop",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Indore/Banglore",
        openRelocate: "Yes",
        message : ""
    },
    {
        id: 3,
        name: "Mark Miller",
        designation: "UI Specialist",
        technologies: "XD,Sketch,Figma",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Indore",
        openRelocate: "Yes",
        message : ""
    },
    {
        id: 4,
        name: "Jennifer Parker",
        designation: "Senior UI Specialist",
        technologies: "XD,Sketch,Figma",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Hyderabad",
        openRelocate: "Yes",
        message : ""
    },
    {
        id: 5,
        name: "Thomas Hall",
        designation: "Lead UI Specialist",
        technologies: "Photoshop",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Hyderabad",
        openRelocate: "Yes",
        message : ""
    },
    {
        id: 6,
        name: "Ronald Baker",
        designation: "Senior Lead UI Specialist",
        technologies: "XD,Sketch,Figma",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Indore",
        openRelocate: "No",
        message : ""
    },
    {
        id: 7,
        name: "Susan Carter",
        designation: "UI Specialist",
        technologies: "XD,Sketch,Figma",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Pune",
        openRelocate: "No",
        message : ""
    },
    {
        id: 8,
        name: "Sarah White",
        designation: "Associate UI Specialist",
        technologies: "Photoshop",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Hyderabad",
        openRelocate: "Yes",
        message : ""
    },
    {
        id: 9,
        name: "James Harris",
        designation: "UI Specialist",
        technologies: "Illustrator",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Indore/Hyderabad",
        openRelocate: "Yes",
        message : ""
    },
    {
        id: 10,
        name: "Jhon White",
        designation: "UI Specialist",
        technologies: "XD,Sketch,Figma",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Hyderabad",
        openRelocate: "Yes",
        message : ""
    },
    {
        id: 11,
        name: "Chritopher Lee",
        designation: "Senior UI Specialist",
        technologies: "Illustrator",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Banglore",
        openRelocate: "No",
        message : ""
    },
    {
        id: 12,
        name: "Lisa Taylor",
        designation: "Senior UI Specialist",
        technologies: "XD,Sketch,Figma",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Pune",
        openRelocate: "NO",
        message : ""
    },
    {
        id: 13,
        name: "Jhon Smith",
        designation: "Senior UI Specialist",
        technologies: "XD,Sketch,Figma",
        skill: "UX,UI,Visual",
        availablity: "3",
        baseLocation: "Indore",
        openRelocate: "NO",
        message : ""
    },
]