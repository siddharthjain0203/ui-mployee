import React, { Fragment, useEffect, useState } from "react";
import CustomTable from "../../components/Table";
import { candidatedCol } from "./components/Columns";
import RowActions from './components/RowAction'
import { candidates } from "../../data/candidates";
import SearchFilter from "./components/SearchFilter";
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { setCandidateList } from '../../store/actionsCreator/candidateActionCreator';
import { Grid, Button } from "@material-ui/core";
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerContainer: {
        overflow: 'auto',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    }
}));

function Candidates(props) {
    const { candidatesList } = props
    const classes = useStyles();
    const [row, setRow] = React.useState(0);
    const [selectedCandidate, setCandidateToDelete] = useState([])
    const [action, setAction] = React.useState({
        open: false,
        disabled: false
    });

    const handleRowAction = (e, row) => {
        console.log("handle row action", row)
    }
    const handleOnHeaderAction = (e, action) => {
        // props.setCandidateList([])
    }

    const onRowActions = (action, row, option) => {
        if (option === "delete") {
            selectedCandidate.push(row.id)
            setCandidateToDelete(selectedCandidate);
        } else {
            setRow(row)
            setAction({ open: true, disabled: true })
        }
    }

    const onCloseRowAction = () => {
        setAction({ open: false, disabled: false })
    }

    const filterData = (searchObject) => {
        const { technology, skills, location, experience } = searchObject
        const filterData = candidatesList.filter(candi => {
            let status = true;
            const tech = candi.technologies.split(',')
            const tempSkill = candi.skill.split(',');
            const tempLoca = candi.baseLocation.split('/');
            if (technology.length || skills.length || location.length)
                status = tech.filter(techno => technology.includes(techno)).length ||
                    tempSkill.filter(obj => skills.includes(obj)).length ||
                    tempLoca.filter(obj => location.includes(obj)).length ||
                    experience === candi.availablity

            return status
        })
        props.setCandidateList(filterData)
    }

    const handleCancel = () => {
        props.setCandidateList(candidates);
    }

    const addCandidateAction = () => {
        setRow(null);
        setAction({ open: true, disabled: false })
    }

    const deleteCandidate = () => {
        const filterData = candidatesList.filter(candi => {
            if (selectedCandidate.includes(candi.id)) {
                return false;
            }
            return true;
        })
        props.setCandidateList(filterData)
    }

    const updateCandidate = (updateValue) => {
        let updateList = candidatesList;
        if (updateValue.id) {
            updateList = updateList.map(data => {
                if (data.id === updateValue.id) {
                    data = updateValue;
                }
                return data;
            })
        } else {
            updateValue['id'] = updateList.length + 1;
            updateList.unshift(updateValue)
        }
        props.setCandidateList(updateList);
    }
    const renderRowAction = () => {
        return <RowActions
            row={row}
            disabled={action.disabled}
            action={action.open}
            onClose={onCloseRowAction}
            updateCandidate={updateCandidate}
        />
    }
    return (
        <Fragment>
            <Grid item sm={2}>
                <Drawer
                    className={classes.drawer}
                    variant="permanent"
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <Toolbar />
                    <div className={classes.drawerContainer}>
                        <SearchFilter setFilter={filterData} cancel={handleCancel} />
                    </div>
                </Drawer>
            </Grid>
            <Grid item sm={10} >
                <div className="table-main-sec">
                    <div className="txt-lft">
                        <Button variant="outlined" color="primary" onClick={addCandidateAction}>Add</Button>
                        <Button variant="outlined" color="primary" onClick={deleteCandidate}>Delete</Button>
                    </div>
                    <CustomTable
                        data={candidatesList || []}
                        columns={candidatedCol}
                        onRowActions={onRowActions}
                        onHeader={handleOnHeaderAction}
                        onRowClick={handleRowAction}
                    />
                    {renderRowAction()}
                </div>
            </Grid>
        </Fragment>
    )
}

const mapPropsToState = ({ candidate }) => ({
    candidatesList: candidate.candidates,
})


export default connect(mapPropsToState, { setCandidateList })(Candidates)