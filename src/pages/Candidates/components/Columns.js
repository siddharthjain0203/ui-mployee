export const candidatedCol = [
    {
        label:"Name",
        field:"name",
        id:1,
        align:"left",
        minWidth: 170,
        visibility:true,
        dataType:"date/number/object/file/string"
    },
    {
        id:2,
        field:"designation",
        label:"Designation",
        align:"center",
        minWidth: 170,
        visibility:true,
        dataType:"date/number/object/file/string"
    },
    {
        label:"Technologies",
        field:"technologies",
        id:1,
        align:"center",
        minWidth: 170,
        visibility:true,
        dataType:"date/number/object/file/string"
    },
    {
        label:"Skill",
        field:"skill",
        id:1,
        align:"center",
        minWidth: 170,
        visibility:true,
        dataType:"date/number/object/file/string"
    },
    {
        label:"Availablity",
        field:"availablity",
        id:1,
        align:"center",
        minWidth: 170,
        visibility:true,
        dataType:"date/number/object/file/string"
    },
    {
        label:"Base Location",
        field:"baseLocation",
        id:1,
        align:"center",
        minWidth: 170,
        visibility:true,
        dataType:"date/number/object/file/string"
    },
    {
        label:"Open Relocate",
        field:"openRelocate",
        id:1,
        align:"center",
        minWidth: 170,
        visibility:true,
        dataType:"date/number/object/file/string"
    }
]