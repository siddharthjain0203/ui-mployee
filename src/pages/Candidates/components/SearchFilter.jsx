import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import Slider from '@material-ui/core/Slider';
import { Button } from '@material-ui/core';
const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 200,
        maxWidth: 200,
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
    noLabel: {
        marginTop: theme.spacing(3),
    },
    alignRight:{
        float:'right'
    },
    inputLabel:{
        textAlign:"left",
        paddingLeft:'15px'
    }
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const technology = [
    'XD Sketch',
    'Photoshop',
    'Illustrator',
    'Sketch',
    'Figma'
];
const skills = [
    'UX Designer',
    'UI Designer',
    'Visual',
    'Research',
    'Graphic'
];
const location = [
    'Indore',
    'Hyderabad',
    'Hyderabad Mindspace',
    'Pune Magarpatta',
    'Pune Hinjewadi'
];

function getStyles(name, personName, theme) {
    return {
        fontWeight:
            personName.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}

export default function SearchFilter(props) {
    const classes = useStyles();
    const theme = useTheme();
    const [selectedTechnology, setTechnology] = React.useState([]);
    const [selectedSkills, setSkills] = React.useState([]);
    const [selectedLocation, setLocation] = React.useState([]);
    const [totalExperience, setExperience] = React.useState(0);

    const handleExperience = (event, newValue) => {
        setExperience(newValue);
    };
    const handleTchnology = (event, newValue) => {
        setTechnology(event.target.value);
    };
    const handleSkills = (event, newValue) => {
        setSkills(event.target.value);
    };
    const handleLocation = (event, newValue) => {
        setLocation(event.target.value);
    };

    const handleDone = () => {
        const filterValue = {
            technology: selectedTechnology,
            skills: selectedSkills,
            location: selectedLocation,
            experience: totalExperience
        }
        props.setFilter(filterValue)
    }

    const resetFiler = () => {
        props.cancel()
    }

  
    return (
        <div className="emp-skill-data">
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-mutiple-chip-label">Technology</InputLabel>
                <Select
                    labelId="demo-mutiple-chip-label"
                    id="demo-mutiple-chip"
                    multiple
                    value={selectedTechnology}
                    onChange={handleTchnology}
                    input={<Input id="select-multiple-chip" />}
                    renderValue={(selected) => (
                        <div className={classes.chips}>
                            {selected.map((value) => (
                                <Chip key={value} label={value} className={classes.chip}/>
                            ))}
                        </div>
                    )}
                    MenuProps={MenuProps}
                >
                    {technology.map((name) => (
                        <MenuItem key={name} value={name} style={getStyles(name, selectedTechnology, theme)}>
                            {name}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-mutiple-chip-label">Skills</InputLabel>
                <Select
                    labelId="demo-mutiple-chip-label"
                    id="demo-mutiple-chip"
                    multiple
                    value={selectedSkills}
                    onChange={handleSkills}
                    input={<Input id="select-multiple-chip" />}
                    renderValue={(selected) => (
                        <div className={classes.chips}>
                            {selected.map((value) => (
                                <Chip key={value} label={value} className={classes.chip} />
                            ))}
                        </div>
                    )}
                    MenuProps={MenuProps}
                >
                    {skills.map((name) => (
                        <MenuItem key={name} value={name} style={getStyles(name, selectedSkills, theme)}>
                            {name}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <InputLabel id="demo-mutiple-chip-label" className={classes.inputLabel}>Total Experience</InputLabel>
            <FormControl className={classes.formControl}>
                <Slider value={totalExperience} onChange={handleExperience}
                    aria-labelledby="continuous-slider"
                    valueLabelDisplay="auto"
                    step={1}
                    min={0}
                    max={25}
                />
            </FormControl>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-mutiple-chip-label">Location</InputLabel>
                <Select
                    labelId="demo-mutiple-chip-label"
                    id="demo-mutiple-chip"
                    multiple
                    value={selectedLocation}
                    onChange={handleLocation}
                    input={<Input id="select-multiple-chip" />}
                    renderValue={(selected) => (
                        <div className={classes.chips}>
                            {selected.map((value) => (
                                <Chip key={value} label={value} className={classes.chip} />
                            ))}
                        </div>
                    )}
                    MenuProps={MenuProps}
                >
                    {location.map((name) => (
                        <MenuItem key={name} value={name} style={getStyles(name, selectedLocation, theme)}>
                            {name}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        <div className={classes.alignRight}>
            <Button variant="outlined" onClick={resetFiler}>Reset</Button>
            <Button variant="contained" color="primary" onClick={handleDone}>Done</Button>
            </div>
        </div>
    );
}