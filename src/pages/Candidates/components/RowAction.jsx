import React, { Fragment, useState, useEffect } from 'react';
import { connect } from "react-redux";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DeleteIcon from '@material-ui/icons/RemoveRedEye';
import DialogContentText from '@material-ui/core/DialogContentText';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { openSnackbar } from '../../../store/actionsCreator/snackbarActionCreator';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));
function CandidateForm(props) {
    const { openSnackbar, action, row, disabled } = props
    const [rowData, setRowData] = useState(row || {
        name: "",
        designation: "",
        technologies: "",
        skill: "",
        availablity: "",
        baseLocation: "",
        openRelocate: "",
        message: ""
    });
    const classes = useStyles();

    useEffect(() => {
        if (row) {
            setRowData(row);
        }
    }, [row])

    const handleTextField = (event) => {
        setRowData({ ...rowData, [event.target.name]: event.target.value });
    }
    const saveData = () => {
        props.updateCandidate(rowData);
        openSnackbar('success', "Message updated successfully!")
        props.onClose()
    };
    
    const handleClose = () => {
        props.onClose()
    };

    return (
        <Dialog
            open={action}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
            className="dialog-emp-detail"
        >
            <Fragment>
                <DialogTitle id="alert-dialog-title"  className="emp-title">{disabled ? "Employee Details" : "Add Employee"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <form className={classes.root} noValidate autoComplete="off">
                            <TextField id="name" name="name" label="Name" onChange={handleTextField} value={rowData.name} disabled={disabled} />
                            <TextField id="skill" name="skill" label="Skill" onChange={handleTextField} value={rowData.skill} disabled={disabled} />
                            <TextField id="openRelocate" name="openRelocate" label="Open to Relocate" onChange={handleTextField} value={rowData.openRelocate} disabled={disabled} />
                            <TextField id="designation" name="designation" label="Designation" onChange={handleTextField} value={rowData.designation} disabled={disabled} />
                            <TextField id="availablity" name="availablity" label="Availablity" onChange={handleTextField} value={rowData.availablity} disabled={disabled} />
                            <TextField id="technologies" name="technologies" label="Technologies" onChange={handleTextField} value={rowData.technologies} disabled={disabled} />
                            <TextField id="baseLocation" name="baseLocation" label="Base Location" onChange={handleTextField} value={rowData.baseLocation} disabled={disabled} />
                            <TextareaAutosize aria-label="minimum height" rowsMin={8} placeholder="Minimum 8 rows" label={"Message"} name={"message"} value={rowData.message} onChange={handleTextField} />
                        </form>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">Cancel</Button>
                    <Button onClick={saveData} color="primary" autoFocus>Done</Button>
                </DialogActions>
            </Fragment>
        </Dialog>
    )
}

export default connect(null, { openSnackbar })(CandidateForm);