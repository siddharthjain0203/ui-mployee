import React from 'react';
import './styles/App.scss';

import { makeStyles, fade } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Grid, Paper } from "@material-ui/core";


import Candidates from "./pages/Candidates";
import store, { persistor } from "./store";
import Layout from './components/Layout';
import Snackbar from './components/common/Snackbar';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },

}));

function App() {
  const classes = useStyles();
  return (
    <div className="App mainQuad">
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <div className={classes.root}>
            <Layout />
            <Grid container className="left-outer-quad">
              <Candidates />
            </Grid>
            <Snackbar />
          </div>
        </PersistGate>
      </Provider>
    </div>
  );
}

export default App;
